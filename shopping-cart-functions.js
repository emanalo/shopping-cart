function addPromo(){
    document.getElementById("totalAmount").innerHTML = "<h3 id='totalAmount'>TOTAL: </h3>";
    document.getElementById("promoCode").innerHTML = "<h3 id='promoCode' class='promo' value='true'>PROMO IS APPLIED</h3>"
}

function addToCart(item) {
    var name = "";
    var price = "";
    if (item == "ult_small"){
        name = "Unlimited 1GB";
        price = "$ " + document.getElementById("smallPrice").value;
    } else if (item == "ult_medium") {
        name = "Unlimited 2GB";
        price = "$ " + document.getElementById("mediumPrice").value;
    } else if (item == "ult_large") {
        name = "Unlimited 5GB";
        price = "$ " + document.getElementById("largePrice").value;
    }  else if (item == "1gb") {
        name = "1GB Data-Pack";
        price = "$ " + document.getElementById("dpPrice").value;
    }
    
    var cartTable = document.getElementById("cartTable").innerHTML;
    cartTable += "<tr><td>" + item + "</td><td>" + name +  "</td><td>" + price + "</td></tr>";
    if (item == "ult_medium") {
        cartTable += "<tr><td>1gb</td><td>1GB Data-Pack</td><td> ---- </td></tr>";
    }

    document.getElementById("cartTable").innerHTML = cartTable;
    disableEnableBtn(false);
}

function calculate(){
    var cartTable = document.getElementById("cartTable");
    var smallItem = 0;
    var mediumItem = 0;
    var largeItem = 0;
    var dpItem = 0;

    // get cart details
    for (var i = 1, row; row = cartTable.rows[i]; i++){
        if (row.cells[0].textContent == "ult_small"){
            smallItem++;
        } else if (row.cells[0].textContent == "ult_medium"){
            mediumItem++;
        } else if (row.cells[0].textContent == "ult_large"){
            largeItem++;
        } else if (row.cells[0].textContent == "1gb"){
            dpItem++;
        }
    }

    var productTable = document.getElementById("productTable");
    var smallPrice = document.getElementById("smallPrice").value;
    var mediumPrice = document.getElementById("mediumPrice").value;
    var largePrice = document.getElementById("largePrice").value;
    var dpPrice = document.getElementById("dpPrice").value;

    // pricing rules
    var toPay = 0;

    // smallItem
    if (smallItem < 3) {
        toPay = smallItem * smallPrice;
    } else {
        var multipleOf3 = (smallItem / 3) | 0;
        var remainder = smallItem % 3;
        toPay = (2 * multipleOf3 * smallPrice) + (remainder * smallPrice);
    }
    
    // mediumItem
    if (mediumItem) {
        toPay += mediumItem * mediumPrice;
    }

    // largeItem
    if (largeItem < 3) {
        toPay += largeItem * largePrice;
    } else {
        var largeDiscountedPrice = document.getElementById("discountedPrice").value;
        toPay += largeItem * largeDiscountedPrice;
    }

    // data pack
    if (dpItem != 0 && mediumItem == 0){
        toPay += dpItem * dpPrice;
    }

    // promo
    var isPromoApplied = document.getElementById("promoCode").textContent;
    if (isPromoApplied != ""){
        var promoDiscount = (document.getElementById("promoDiscount").value) / 100;
        toPay = toPay - (toPay * promoDiscount);
    }
    
    var totalAmount = toPay.toFixed(2);

    document.getElementById("totalAmount").innerHTML = "<h3 id='totalAmount'>TOTAL: $" + totalAmount + "</h3>";
}

function clearCart(){
    document.getElementById("cartTable").innerHTML = "<table id='cartTable'><tr><th>CODE</th><th>NAME</th><th>PRICE</th></tr></table>";
    document.getElementById("totalAmount").innerHTML = "<h3 id='totalAmount'>TOTAL: </h3>";
    document.getElementById("promoCode").innerHTML = "<h3 id='promoCode'></h3>"
    disableEnableBtn(true);
}

function disableEnableBtn(value){
    var checkoutBtn = document.getElementById("checkout");
    var promoBtn = document.getElementById("promo");
    if (value){
        checkoutBtn.disabled = true;
        checkoutBtn.classList.add("disabled");
        promoBtn.disabled = true;
        promoBtn.classList.add("disabled");
    } else {
        checkoutBtn.disabled = false;
        checkoutBtn.classList.remove("disabled");
        promoBtn.disabled = false;
        promoBtn.classList.remove("disabled");
    }
}
