# TEST STEPS

Download a copy of the shopping-cart repository, extract the downloaded compressed file and then run the index.html.

![repository](/test-results/repository.png)


## Case One

1. Add 3 Unlimited 1GB (expected result: 3 Unlimited 1GB will be added in the Table below)

2. Add 1 Unlimited 5GB (expected result: 1 Unlimited 5GB will be added in the Table below)

3. Select the Checkout button (expected result: Total should be equal to $94.70)

![case-one](/test-results/case-one.png)


## Case Two

NOTE: Select the Clear button before adding new items.

1. Add 2 Unlimited 1GB (expected result: 2 Unlimited 1GB will be added in the Table below)

2. Add 4 Unlimited 5GB (expected result: 4 Unlimited 5GB will be added in the Table below)

3. Select the Checkout button (expected result: Total should be equal to $209.40)

![case-two](/test-results/case-two.png)


## Case Three

NOTE: Select the Clear button before adding new items.

1. Add 1 Unlimited 1GB (expected result: 1 Unlimited 1GB will be added in the Table below)

2. Add 2 Unlimited 2GB (expected result: 2 Unlimited 2GB and 2 1GB Data-Pack will be added in the Table below)

3. Select the Checkout button (expected result: Total should be equal to $84.70)

![case-three](/test-results/case-three.png)


## Case Four

NOTE: Select the Clear button before adding new items.

1. Add 1 Unlimited 1GB (expected result: 1 Unlimited 1GB will be added in the Table below)

2. Add 1 1GB Data-Pack (expected result: 1 1GB Data-Pack will be added in the Table below)

3. Select the PROMO CODE: 1<3AMAYSIM button to note the promo discount when checked out

4. Select the Checkout button (expected result: Total should be equal to $31.32)

![case-four](/test-results/case-four.png)


## Other Scenario

NOTE: Select the Clear button before adding new items.

1. Update the price of  Unlimited 1GB to 25

2. Update the price of Unlimited 5GB to 45

3. Add 3 Unlimited 1GB (expected result: 3 Unlimited 1GB will be added in the Table below)

4. Add 1 Unlimited 5GB (expected result: 1 Unlimited 5GB will be added in the Table below)

5. Select the Checkout button (expected result: Total should be equal to $95.00)

![case-five](/test-results/case-five.png)


NOTE: Select the Clear button before adding new items.

1. Update the special offer 2 to 40

2. Add 2 Unlimited 1GB (expected result: 2 Unlimited 1GB will be added in the Table below)

3. Add 4 Unlimited 5GB (expected result: 4 Unlimited 5GB will be added in the Table below)

4. Select the Checkout button (expected result: Total should be equal to $210.00)

![case-six](/test-results/case-six.png)


NOTE: Select the Clear button before adding new items.

1. Updated the price for Unlimited 2GB to 30

2. Add 1 Unlimited 1GB (expected result: 1 Unlimited 1GB will be added in the Table below)

3. Add 2 Unlimited 2GB (expected result: 2 Unlimited 2GB and 2 1GB Data-Pack will be added in the Table below)

4. Select the Checkout button (expected result: Total should be equal to $85.00)

![case-seven](/test-results/case-seven.png)


NOTE: Select the Clear button before adding new items.

1. Update the price for 1GB Data-Pack to 10

2. Update the promo discount to 20

3. Add 1 Unlimited 1GB (expected result: 1 Unlimited 1GB will be added in the Table below)

4. Add 1 1GB Data-Pack (expected result: 1 1GB Data-Pack will be added in the Table below)

5. Select the Checkout button (expected result: Total should be equal to $35.00)

![case-eight-1](/test-results/case-eight-1.png)

6. Select the PROMO CODE: 1<3AMAYSIM button to note the promo discount when checked out (expected result: the total from previous checkout will be removed)

![case-eight-2](/test-results/case-eight-2.png)

7. Select the Checkout button (expected result: Total should be equal to $28.00)

![case-eight-3](/test-results/case-eight-3.png)
